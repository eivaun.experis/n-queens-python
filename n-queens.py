import numpy as np

class Board:
    def __init__(self, n=8) -> None:
        # The number of queens and size of the board
        self.n = n

        # The order of which column/row we try to place the queens in first
        self.index_order = np.arange(8)
        np.random.shuffle(self.index_order)

        # The position of each queen.
        # A queen in row 2 column 4 is indicated by queens[2] == 4
        self.queens = np.zeros(n)

        # Keeps track of the occupied columns.
        # Column 2 is occupied if occupied_cols[2]==True
        self.occupied_cols = np.repeat(False, n)

        # The number of attacks on each column and diagonal
        self.attacks_in_col = np.zeros(n)
        self.attacks_in_diagonal_pos = np.zeros(n * 2 - 1)
        self.attacks_in_diagonal_neg = np.zeros(n * 2 - 1)
        self.neg_offset = n

    def print(self) -> None:
        print("┌─", "──" * self.n, "┐", sep="")
        for r in range(self.n):
            print("│ ", end="")
            for c in range(self.n):
                print("Q" if self.queens[r] == c else ".", end=" ")
            print("│")
        print("└─", "──" * self.n, "┘", sep="")

    def get_num_attacks(self, row, col) -> int:
        attacks = 0
        attacks += self.attacks_in_diagonal_pos[col + row]
        attacks += self.attacks_in_diagonal_neg[col - row + self.neg_offset]
        return attacks

    def place_queen(self, row, col):
        self.queens[row] = col
        self.occupied_cols[col] = True
        self.attacks_in_diagonal_pos[col + row] += 1
        self.attacks_in_diagonal_neg[col - row + self.neg_offset] += 1

    def remove_queen(self, row, col):
        self.occupied_cols[col] = False
        self.attacks_in_diagonal_pos[col + row] -= 1
        self.attacks_in_diagonal_neg[col - row + self.neg_offset] -= 1

    def place_queens_rec(self, depth) -> bool:
        # Check if we've placed all the queens
        if depth == self.n:
            return True

        # Edges -> middle
        row = self.index_order[self.n - depth - 1]

        # Place the new queen in each square of the row
        for c in range(self.n):
            # Middle -> edges
            col = self.index_order[c]

            # Can't have two queens in the some column
            if self.occupied_cols[col]:
                continue

            # If we can place it in this square
            if self.get_num_attacks(row, col) == 0:
                self.place_queen(row, col)
                # Go on to the next queen
                if self.place_queens_rec(depth + 1):
                    return True

                # If the following queens could not be placed, we must try a new position for this one
                self.remove_queen(row, col)
                self.occupied_cols[col] = False

        # The queen could not be placed in any square in this row
        return False

if __name__ == "__main__":
    b = Board(8)
    b.place_queens_rec(0)
    b.print()
